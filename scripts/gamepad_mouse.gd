extends Node2D

var active = true
var m_pos

func _ready():
	Input.set_custom_mouse_cursor(preload("res://assets/art/circle.png"), 0, Vector2(16,16))

func _process(delta):
	if active:
		var c_x = Input.get_joy_axis(0,2)
		var c_y = Input.get_joy_axis(0,3)
		var c_speed = 15
		var c_move = Vector2(c_x,c_y) * c_speed
		m_pos = get_viewport().get_mouse_position()
		
		if c_x != 0 or c_y != 0:
			m_pos += c_move
			m_pos.x = clamp(m_pos.x, 0, get_viewport_rect().size.x)
			m_pos.y = clamp(m_pos.y, 0, get_viewport_rect().size.y)
		
			get_viewport().warp_mouse(m_pos)
		
		if Input.is_joy_button_pressed(0, 0):
			call_deferred("click", m_pos)

func click(pos):
	var click = InputEventMouseButton.new()
	click.set_button_index(1)
	click.pressed = true
	click.position = pos
	Input.parse_input_event(click)
	click.pressed = false
	click.position = pos
	Input.parse_input_event(click)
	get_viewport().warp_mouse(pos)