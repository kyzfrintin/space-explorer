extends Node2D

var distance = 0
var mass = 100
var type = "moon"
var center = Vector2(0,0)
var has_cloud = true
var clouds = []
var moon_stats
var colours
var num
onready var cloud_tex = [preload("res://assets/art/clouds.png"),preload("res://assets/art/clouds2.png"),
preload("res://assets/art/clouds3.png")]
onready var tex = [[preload("res://assets/art/planet_white.png"),preload("res://assets/art/planet_black.png")],
[preload("res://assets/art/planet_white2.png"),preload("res://assets/art/planet_black2.png")],
[preload("res://assets/art/planet_white3.png"),preload("res://assets/art/planet_black3.png")]]
onready var planet = get_node("../../")
onready var sun = planet.get_node("../../")
onready var game = sun.get_node("../../")

func recreate(data):
	$TextureButton.texture_normal = data["textures"][0]
	$Sprite.texture = data["textures"][1]
	var sca = data["size"]
	name = data["mname"]
	center = Vector2(2000,2000) * sca
	$TextureButton.rect_scale = Vector2(sca,sca)
	$TextureButton.rect_position = -center
	$Sprite.scale = Vector2(sca,sca)
	$Sprite2.scale = Vector2(sca,sca)
	mass = 20*sca

func save_stats():
	var cs = []
	for i in clouds:
		var cloud_data = {
			tex = i.texture,
			size = i.scale.x,
			colour = i.modulate
			}
	moon_stats = {
		body_type = "moon",
		mname = name,
		colours = colours,
		size = $TextureButton.rect_scale.x,
		clouds = cs,
		textures = [$TextureButton.texture_normal, $Sprite.texture],
		distance = position.y,
		rot = get_parent().rotation
		}
	return moon_stats

func generate():
	randomize()
	var itex = randi() % tex.size()
	$TextureButton.texture_normal = tex[itex][0]
	$Sprite.texture = tex[itex][1]
	var sca = rand_range(0.010,1.800)
	center = Vector2(2000,2000) * sca
	$TextureButton.rect_scale = Vector2(sca,sca)
	$TextureButton.rect_position = -center
	$Sprite.scale = Vector2(sca,sca)
	$Sprite2.scale = Vector2(sca,sca)
	mass = 20*sca
	recolour()
	if rand_range(0,1) < 0.5:
		generate_clouds(sca)
		move_child($Sprite2,get_child_count())

func generate_clouds(sca):
	var c = randi() % 6
	var csca = 1.008
	for i in c:
		var cloud = Sprite.new()
		cloud.texture = cloud_tex[randi() % cloud_tex.size()]
		cloud.scale = Vector2(sca * csca, sca * csca)
		cloud.rotate(rand_range(0,360))
		csca += rand_range(0.006,0.012)
		add_child(cloud)
		cloud.set_owner(self)
		var shader = preload("res://assets/shaders/nebula_ripple.shader")
		var nmat = ShaderMaterial.new()
		cloud.set_material(nmat)
		var mat = cloud.get_material()
		mat.shader = shader
		mat.set_shader_param("speed", csca * 0.02)
		mat.set_shader_param("volume", csca * rand_range(0.5,1.2))
		mat.set_shader_param("volume1", csca * rand_range(0.5,1.2))
		mat.set_shader_param("volume2", csca * rand_range(0.5,1.2))
		mat.set_shader_param("volume3", csca * rand_range(0.5,1.2))
		clouds.append(cloud)
		cloud.modulate = Color(rand_range(0.7,1),rand_range(0.7,1),rand_range(0.7,1),rand_range(0.02,0.30))

func _process(delta):
	if clouds.size() > 0:
		for i in clouds:
			i.rotate(0.001 / i.scale.x)
	$Sprite2.look_at(sun.global_position)
	
func recolour():
	randomize()
	var col1 = Color(rand_range(0,1),rand_range(0,1),rand_range(0,1),1)
	var col2 = Color(rand_range(0,1),rand_range(0,1),rand_range(0,1),1)
	colours = [col1,col2]
	$TextureButton.modulate = col1
	$Sprite.modulate = col2

func _on_TextureButton_mouse_entered():
	game.update_label(self)

func _on_TextureButton_mouse_exited():
	game.hide_label()
