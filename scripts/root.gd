extends Node2D

var map_open = false
var body_hovered

var shake = false
var shake_num = 0
var shake_amnt = 40
var tweening_nebs = []
var colours = [Color(0.0562, 0.0258, 0.4140),Color(0.3803,0,0.0352),Color(0.0156,0.4627,0.4627),
Color(0.4274,0.0156,0.4627),Color(0.0705,0.4235,0.0117),Color(0.4117,0.4235,0.0117),
Color(0.482353, 0.1137, 0.6862),Color(0.871094, 0.2041, 0.3135),Color(0.898438, 0.4888, 0.2667)]
var space_cam_limits = [-1410065408, -1410065408, 1410065408, 1215752192]
var cam_zoom = 10
var drones = 0
var threat = 0
var battle = false
var planet_icons = []
var belt_icons = []
var zoom_range = "close"
var planets_visible = true
onready var wep_label = $UI/weplabel
onready var player = get_node("player")
onready var p_cam = player.cam
onready var bullets = get_node("bullets")
onready var fx = get_node("fx")
onready var sun = get_node("suns").get_child(0)
onready var ast_res = preload("res://scenes/crate.tscn")
onready var nebs = [get_node("bg_parallax/nebula_far/1"),get_node("bg_parallax/nebula_far/2"),
get_node("bg_parallax/nebula_mid/2"),get_node("bg_parallax/nebula_mid/3"),
get_node("bg_parallax/nebula_close/3"),get_node("bg_parallax/nebula_close/4")]

func _process(delta):
	var m_pos = get_viewport().get_mouse_position()
	
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit()
	if Input.is_action_just_pressed("ui_click") and map_open:
		if body_hovered != null:
			player.autopiloting = true
			player.dest = body_hovered
			player.damping = false
			$auto_on.play()
		else:
			if player.autopiloting:
				player.autopiloting = false
				player.dest = null
				player.damping = true
				$auto_off.play()
	if Input.is_action_just_pressed("ui_rightclick"):
		if body_hovered != null:
			var dis = player.global_position.distance_to(body_hovered.global_position)
			if dis < 100000:
				scene_holder.land(body_hovered)
		
	$UI/Panel.rect_position = m_pos + Vector2(-64, 32)
	if map_open:
		$UI/icons/player_indicator.position = player.get_global_transform_with_canvas().origin
		$UI/icons/player_indicator.rotation = player.rotation
		if !$UI/cam_coord.visible:
			$UI/cam_coord.visible = true
		else:
			var start_pos = get_global_mouse_position() + (Vector2(250,250) * $suns.get_child(0).get_node("TextureButton").rect_scale.x)
			var pos = Vector2(0,0)
			pos.x = floor(start_pos.x * 0.00001)
			pos.y = floor(start_pos.y * 0.00001)
			$UI/cam_coord.text = str(pos)
		if $UI/weplabel.visible:
			$UI/weplabel.visible = false
		if $UI/healthlabel.visible:
			$UI/healthlabel.visible = false
		update_planet_icons()
	else:
		if $UI/cam_coord.visible:
			$UI/cam_coord.visible = false
		if !$UI/weplabel.visible:
			$UI/weplabel.visible = true
		if !$UI/healthlabel.visible:
			$UI/healthlabel.visible = true
	if shake && shake_num > 0:
		shake_num -= 1
		p_cam.offset = Vector2(rand_range(-shake_amnt, shake_amnt), rand_range(-shake_amnt, shake_amnt))
	else:
		p_cam.offset = Vector2(0,0)
	
	match player.weapon:
		"Laser":
			$UI/weplabel/reload.max_value = player.get_node("attack_timer").wait_time
			$UI/weplabel/reload.value = player.get_node("attack_timer").time_left
		"Missile":
			$UI/weplabel/reload.max_value = player.get_node("missile_timer").wait_time
			$UI/weplabel/reload.value = player.get_node("missile_timer").time_left

func shake(intensity):
	shake = true
	shake_num = 20
	shake_amnt = intensity

func enter_battle():
	battle = true
	$battle_enter.play()
	$UI/danger.visible = true
	yield($explore_playlist, "bar")
	$explore_playlist.stop($explore_playlist.current_song_num)
	$battle_playlist.quickplay(0)

func leave_battle():
	battle = false
	$battle_leave.play()
	$UI/danger.visible = false
	yield($battle_playlist, "bar")
	$battle_playlist.stop($battle_playlist.current_song_num)
	$explore_playlist.quickplay(0)

func update_threat():
	$UI/danger/threat.value = threat
	
func create_planet_icons():
	var sun = $suns.get_child(0)
	$UI/icons/sun_icon.modulate = sun.get_node("TextureButton").modulate
	for i in sun.planet_nodes:
		var pi = Sprite.new()
		pi.texture = preload("res://assets/art/bigcircle.png")
		pi.modulate = i.colours[0]
		var sca = (i.get_node("TextureButton").rect_scale.x)/5
		pi.scale = Vector2(sca,sca)
		$UI/icons.add_child(pi)
		planet_icons.append(pi)
	for i in sun.belts:
		var bi = Sprite.new()
		bi.texture = preload("res://assets/art/ring.png")
#		var sca = (i.dis/595)
#		bi.scale = Vector2(sca,sca)
		$UI/icons.add_child(bi)
		belt_icons.append(bi)

func update_planet_icons():
	var sun = $suns.get_child(0)
	var sunpos = sun.get_global_transform_with_canvas().origin + (sun.center / (cam_zoom * 0.65))
	$UI/icons/sun_icon.position = sunpos
	for i in planet_icons:
		var num = planet_icons.find(i)
		var planet = sun.planet_nodes[num]
		i.position = planet.get_global_transform_with_canvas().origin
	for i in belt_icons:
		var num = belt_icons.find(i)
		var belt = sun.belts[num]
		var sca = (belt.dis) / (cam_zoom * 400)
		i.position = sunpos
		i.scale = Vector2(sca,sca)

func _ready():
	if scene_holder.system_stats["sname"] == "null":
		var planet = get_random_planet()
		var start_pos
		if planet:
			start_pos = planet.global_position + Vector2(20000,10000)*planet.scale.x
		else:
			start_pos = Vector2(rand_range(-20000,20000),rand_range(-20000,20000))
		player.pos = start_pos
	else:
		player.pos = scene_holder.system_stats["player"]["pos"]
		player.weapon = scene_holder.system_stats["player"]["wep"]
	
	yield(sun, "finished_naming")
	$UI/Label.text = (sun.name + ' system')
	$UI/Label/Label2.text = (str(sun.planet_num) + ' planets, ' + str(sun.belts.size()) + ' asteroid belts')
	$UI/AnimationPlayer.play("name_fade")
	toggle_planets(false)
	
	
func spawn_asteroids(pos,num,spawn_range,size=0,a_root=null):
	for i in range(num):
		randomize()
		var crate = ast_res.instance()
		var c_pos = Vector2(rand_range(pos.x-spawn_range,pos.x+spawn_range),rand_range(pos.y-spawn_range,pos.y+spawn_range))
		if size == 0:
			crate.size = rand_range(0,10)
		else:
			crate.size = size * rand_range(0.05,0.600)
		crate.rescale(crate.size)
		crate.position = c_pos
		call_deferred("create_asteroid", crate, a_root)
		var v = pos.direction_to(c_pos)*rand_range(10,5000)
		crate.linear_velocity = v

func create_asteroid(ast,a_root):
	a_root.add_child(ast)

func get_random_planet():
	for i in get_node("suns").get_child(0).planet_nodes:
		if !i.occupied:
			return i

func update_label(body):
	if !map_open: return
	var text = ""
	var size = 81
	match body.type:
		"sun":
			text = (body.name + '\n' + str(body.planet_num) + ' planets')
		"planet":
			var occ = ""
			if !body.occupied:
				occ = "(unsettled)"
			else:
				occ = "(settled)"
			text = (body.name + '   ' + occ + '\n' + str(body.m_roots.size()) + ' satellites')
		"moon":
			size = 47
			text = body.name
		"space_station":
			size = 47
			text = (body.planet.name + ' Station ' + str(body.num))
	
	body_hovered = body
	$UI/Panel.rect_size.y = size
	$UI/Panel/Label.text = text
	$UI/Panel.visible = true

func toggle_planets(tog):
	var sun = $suns.get_child(0)
	if tog:
		for i in planet_icons:
			i.show()
		for i in belt_icons:
			i.show()
		for i in sun.planet_nodes:
			i.toggle_planet(false)
		for i in sun.belts:
			i.get_node("pivot").hide()
		sun.get_node("TextureButton").hide()
		$UI/icons/sun_icon.show()
		planets_visible = false
	else:
		for i in planet_icons:
			i.hide()
		for i in belt_icons:
			i.hide()
		for i in sun.planet_nodes:
			i.toggle_planet(true)
		for i in sun.belts:
			i.get_node("pivot").show()
		sun.get_node("TextureButton").show()
		$UI/icons/sun_icon.hide()
		planets_visible = true

func zoom(zoom):
	if zoom > 1000:
		if planets_visible:
			toggle_planets(true)
	else:
		if !planets_visible:
			toggle_planets(false)
	cam_zoom = zoom
	var b = (zoom / 3000)
	b *= 1.2
	b = clamp(b, 1, 9)
	$CanvasLayer/ColorRect.get_material().set_shader_param("amount", b)
	
func hide_label():
	body_hovered = null
	$UI/Panel.visible = false

func modulate_nebula():
	var neb = nebs[randi() % nebs.size()]
	var tweening = (tweening_nebs.find(neb,0) != -1)
	if !tweening:
#		var col = Color(rand_range(0,1),rand_range(0,1),rand_range(0,1),1)
		var col = colours[randi() % colours.size()]
		$bg_parallax/modulate_tween.interpolate_property(neb,'modulate',neb.modulate,col,rand_range(15,40),Tween.TRANS_LINEAR,Tween.EASE_IN)
		$bg_parallax/modulate_tween.start()
		$bg_parallax/modulate_timer.start(rand_range(10,20))
		tweening_nebs.append(neb)
	else:
		$bg_parallax/modulate_timer.start(1)

func end_modulate(object, key):
	tweening_nebs.remove(tweening_nebs.find(object))
