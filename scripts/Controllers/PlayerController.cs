using Godot;
using System;

class PlayerController : RigidBody2D {
  [Export]
  public float thrustStrength = 300f;
  [Export]
  public float maxSpeed = 6f;
  [Export]
  public float turnSpeed = 2.6f;
  
  private Node[] emitters;
  private Node boostEmit;
  private Node thrustStart;
  private Node thrustLoop;
  private Node thrustRumble;
  private Node boostSound;

  

  public override void _Ready() {
    emitters = new Node[2] {
      GetNode("smoke"),
      GetNode("fire")
    };
    boostEmit = GetNode("boost");
    thrustStart = GetNode("sounds/thrust_off");
    thrustLoop = GetNode("sounds/move_loop");
    thrustRumble = GetNode("sounds/thrust_rumble");
    boostSound = GetNode("sounds/boost");
  }

  public override void _PhysicsProcess(float delta) {
    ProcessMovement();
  }

  private void ProcessMovement() {
    float xAxis = Input.GetJoyAxis(0, 0);
    float yAxis = Input.GetJoyAxis(0, 1);

    if (Input.IsActionPressed("gp_left"))
      xAxis = -1f;
    if (Input.IsActionPressed("gp_right"))
      xAxis = 1f;
    if (Input.IsActionPressed("gp_up"))
      yAxis = -1f;
    if (Input.IsActionPressed("gp_down"))
      yAxis = 1f;

    if (xAxis != 0 || yAxis != 0) {
      
      AddCentralForce(new Vector2(
        xAxis * thrustStrength,
        yAxis * thrustStrength
      ));
    }
  }
}