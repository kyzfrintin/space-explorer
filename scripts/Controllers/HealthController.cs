using Godot;

class HealthController : Node2D {
  [Export]
  public float maxHealth = 100f;
  public float health;
  [Export]
  public float armor = 0f; // Not yet implemented.
  public float shield;
  [Export]
  public float maxShield = 0f;
  [Export]
  public float overShields = 0f;
  [Export]
  public float overHealEfficiency = 1.0f; // Overheal is multiplied by this amount.
  [Export]
  public float regenRate = 0.1f; // Multiple of max HP to heal by per second.
  [Export]
  public float regenPause = 2f; // Amount of seconds to pause before regenerating after taking damage.
  [Export]
  public bool vulnerable = true;
  [Export]
  public bool unkillable = false;

  private Timer regenTimer;
  private bool regenerating;
  
  [Signal]
  delegate void OnKill(DamageInfo damageInfo);
  [Signal]
  delegate void OnHit(DamageInfo damageInfo);
  [Signal]
  delegate void OnHeal(HealInfo healInfo);
  [Signal]
  delegate void OnRegen();
  [Signal]
  delegate void OnRegenDelta();

  public override void _Ready() {
    // Make sure that health and shields are maxxed on spawn.
    health = maxHealth;
    shield = maxShield;
    regenTimer = GetNode<Timer>("Timer");
    regenTimer.Connect("timeout", this, nameof(RegenTimeout));
  }

  public override void _Process(float delta) {
    if (regenerating && health < maxHealth) {
      health += maxHealth * regenRate * delta;
      EmitSignal(nameof(OnRegenDelta));
      if (health > maxHealth) health = maxHealth;
    }
  }

  private void RegenTimeout() {
    regenerating = true;
    EmitSignal(nameof(OnRegen));
  }

  public void Hit(DamageInfo damageInfo) {
    if (!vulnerable) return;
    if (damageInfo.amount > 0) {
      damageInfo.AddVictim(GetParent() as Node2D);
      float amount = damageInfo.amount;
      float damageOvershieldBy = Mathf.Clamp(amount, 0f, overShields);
      amount -= damageOvershieldBy;
      float damageShieldBy = Mathf.Clamp(amount, 0f, shield);
      amount -= damageShieldBy;
      overShields -= damageOvershieldBy;
      shield -= damageShieldBy;
      health -= damageInfo.amount;
      if (health <= 0 && damageInfo.canKill) Kill(damageInfo);
      else if (health <= 0) health = 1;
      regenerating = false;
      regenTimer.Start(regenPause);
      EmitSignal(nameof(OnHit), damageInfo);
    } else return;
  }

  public void Kill(DamageInfo damageInfo) {
    if (unkillable) return;
    else if (health > 0) {
      health = 0;
      damageInfo.AddVictim(GetParent() as Node2D);
    }
    EmitSignal(nameof(OnKill), damageInfo);
  }

  public void Heal(HealInfo healInfo) {
    if (healInfo.amount > 0) {
      healInfo.AddTarget(GetParent() as Node2D);
      if (healInfo.amount >= (maxHealth - health)) {
        float amount = healInfo.amount;
        float healHealthBy = Mathf.Clamp(amount, 0f, maxHealth - health);
        amount -= healHealthBy;
        float healShieldsBy = Mathf.Clamp(amount, 0f, maxShield - shield);
        amount -= healShieldsBy;
        float overHealBy = 0f;
        if (healInfo.canOverheal) {
          overHealBy = Mathf.Clamp(amount * overHealEfficiency, 0f, maxHealth - (health - maxHealth));
          amount -= overHealBy;
        }
        float overShieldBy = Mathf.Clamp(amount, 0f, maxShield - overShields);
        health += healHealthBy;
        shield += healShieldsBy;
        if (healInfo.canOverheal) {
          health += overHealBy;
        }
        if (healInfo.canOvershield) {
          overShields += overShieldBy;
        }
        EmitSignal(nameof(OnHeal), healInfo);
      } else health += healInfo.amount;
    }
  }
}
