extends Node2D

var target : Object
var direction : Vector2
var game : Object
export var damage : float
export var max_range : float
export var cooldown : float
var can_fire = true
var spos : Vector2
var ppos : Vector2

onready var bullet_res = preload("res://scenes/bullet.tscn")

func _process(delta):
	ppos = target.global_position
	spos = global_position
	var dis = spos.distance_to(ppos)
	if dis < max_range:
		look_at(ppos)
		if can_fire:
			can_fire = false
			fire()

func _ready():
	$Timer.wait_time = cooldown
	if get_parent().is_in_group("enemies"):
		self.add_to_group("enemies")

func fire():
	$Timer.start()
	var bullet = bullet_res.instance()
	var dir = spos.direction_to(ppos)
	bullet.position = spos
	bullet.dir = dir
	bullet.modulate = Color(0.678, 0.070, 0.070)
	bullet.damage = damage
	bullet.friendly = false
	game.get_node("bullets").add_child(bullet)

func reload():
	can_fire = true
