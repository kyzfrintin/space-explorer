extends Node2D

onready var sun_res = preload("res://scenes/sun.tscn")

export(int) var suns = 1500
export(int) var num = 1000000
var positions = []

var names : Array = []

var star_names_begin : Array = ["Traza", "Stulni", "Betel", "Thurva", "Jurta",
"Bunglo", "Reter", "Keter", "Nova", "Stella", "Melgen", "Alde", "Fomal", "Cano",
"Den", "Fhana", "Luto", "Pergo", "Penem", "Ceva", "Phae", "Alta", "Anta", "Ache",
"Alg", "Urs", "Alni", "Thub", "Poll", "Bella", "Cap", "Thuva", "Pril", "Far", "Prox",
"Grael", "Kaza", "Toa", "Rala"
]

var star_names_end : Array = ["lazen", "fah", "jalia", "geuse", "o", "a", "e",
"gret", "hur", "len", "cron", "ri", "baran", "haut", "pus", "dra", "lia", "phae",
"pene", "rae", "jool", "menem", "laar", "ir", "res", "mar", "ol", "ae", "tak", "lam",
"ui", "in", "haar", "an", "ux", "trix", "ella", "ima", "tae", "dren", "lum", "pol",
"der", "ron", "toa", "zar"
]

#func _ready():
#	randomize()
#	for i in suns:
#		var pos = Vector2(rand_range(-num,num),rand_range(-num,num))
#		if positions.size() > 1:
#			if _check_pos(pos):
#				var sun = sun_res.instance()
#				sun.position = pos
#				add_child(sun)
#		else:
#			var sun = sun_res.instance()
#			sun.position = pos
#			add_child(sun)
#		positions.append(pos)

func _check_pos(pos):
	for i in positions:
		if pos.distance_to(i) < 125000:
			return false
	return true

func try_name(star):
	var new_name = ran_name()
	if !name_taken(new_name):
		star.star_name = new_name
		star.name_chosen = true
		names.append(new_name)
		
func ran_name():
	var ran_name = str(star_names_begin[randi() % star_names_begin.size() - 1] + star_names_end[randi() % star_names_end.size() - 1])
	return ran_name

func name_taken(tname):
	var ind = names.find(tname)
	if ind != -1:
		return true
	else:
		return false