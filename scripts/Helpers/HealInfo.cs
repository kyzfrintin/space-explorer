using Godot;
using System;
using System.Collections.Generic;

// public enum HealType {
//   Normal,
//   CanOverheal,
//   OverhealOnly,
//   CanHealShields,
//   ShieldsOnly,
//   CanOvershield,
//   OvershieldOnly,
//   CanHealBoth
// }

public class HealInfo {
  public Node2D healerNode;
  public List<Node2D> targetNodes;
  public float amount;
  // public HealType type;
  public bool canOverheal;
  public bool canOvershield;

  HealInfo(Node2D healer, float heal, bool overheal = false, bool overshield = false) {
    healerNode = healer;
    amount = heal;
    canOverheal = overheal;
    canOvershield = overshield;
  }

  public void AddTarget (Node2D target) {
    if (!targetNodes.Contains(target))
      targetNodes.Add(target);
    else return;
  }
}