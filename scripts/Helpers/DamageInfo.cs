using Godot;
using System;
using System.Collections.Generic;

public class DamageInfo {
  public Node attackerNode;
  public float amount;
  public List<Node> victimNodes = new List<Node>();
  public bool canKill = true;

  DamageInfo(Node attacker, float damage, bool killing = true) {
    attackerNode = attacker;
    amount = damage;
    canKill = killing;
  }

  public void AddVictim(Node2D victim) {
    if (!victimNodes.Contains(victim))
      victimNodes.Add(victim);
    else return;
  }
}