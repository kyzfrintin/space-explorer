using Godot;
using System;
using System.Collections.Generic;

class WeightedSelection<T> {
  public float totalWeight {
    get {
      float i = 0;
      foreach (float weight in weights.Values) {
        i += weight;
      }
      return i;
    }
  }

  public Dictionary<T, float> weights;

  public int seed;

  public WeightedSelection(int seed, Dictionary<T, float> weights) {
    this.weights = weights;
    this.seed = seed;
  }

  public WeightedSelection(Dictionary<T, float> weights) {
    seed = new RandomNumberGenerator().RandiRange(Int32.MinValue, Int32.MaxValue);
    this.weights = weights;
  }

  public T Next() {
    Random random = new Random(seed);
    float num = (float)random.NextDouble() * totalWeight;
    foreach (T key in weights.Keys) {
      num -= weights[key];
      if (num < weights[key]) {
        return key;
      }
      else continue;
    }
    // This code should be unreachable.
    throw new Exception("Reached unreachable code.");
  }

  public void Add(T item, float weight) {
    if (weights.ContainsKey(item)) {
      weights[item] += weight;
    }
    else {
      weights.Add(item, weight);
    }
  }
}