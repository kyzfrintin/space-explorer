extends Node2D

onready var planet_res = preload("res://scenes/planet.tscn")
onready var ast_res = preload("res://scenes/crate.tscn")
onready var dud_asteroid_res = preload("res://scenes/dud_asteroid.tscn")

var p_roots = []
var planet_nodes = []
var belt_nodes = []
var name_chosen = false
var star_name : String
var naming = true
var planet_num : int = 0
var belts = []
var distances = []
var center = Vector2(0,0)
var colours = [Color(1,0,0), Color(0,0,1)]
var nebcols = [Color(0.0562, 0.0258, 0.4140),Color(0.3803,0,0.0352),Color(0.0156,0.4627,0.4627),
Color(0.4274,0.0156,0.4627),Color(0.0705,0.4235,0.0117),Color(0.4117,0.4235,0.0117),
Color(0.482353, 0.1137, 0.6862),Color(0.871094, 0.2041, 0.3135),Color(0.898438, 0.4888, 0.2667)]
var stats = {}

var letters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q",
"R", "S", "T"]
var type = "sun"

onready var game = get_node("../../")
onready var nebs = [get_node("../../bg_parallax/nebula_far/1"),get_node("../../bg_parallax/nebula_far/2"),
get_node("../../bg_parallax/nebula_mid/2"),get_node("../../bg_parallax/nebula_mid/3"),
get_node("../../bg_parallax/nebula_close/3"),get_node("../../bg_parallax/nebula_close/4")]

signal finished_naming

func _ready():
	if scene_holder.system_stats["sname"] != "null":
		name_chosen = true
		naming = false
	scene_holder.system_loaded(self)
		

func recreate(data):
	var bodies = data["bodies"]
	name = data["sname"]
	star_name = name
	$TextureButton.modulate = data["colour"]
	var sca = data["size"]
	center = Vector2(250,250) * sca
	$TextureButton.rect_scale = Vector2(sca,sca)
	var dis
	for i in bodies:
		dis = i["distance"]
		match i["body_type"]:
			"asteroid":
				var ast_root = Node2D.new()
				add_child(ast_root)
				ast_root.set_owner(self)
				ast_root.position = center
				ast_root.set_script(preload("res://scripts/ast_root.gd"))
				ast_root.mass = dis * -0.000000001
				create_asteroid_belt(Vector2(0,0),dis,0,360,ast_root)
				p_roots.append(ast_root)
				belts += 1
			"planet":
				var planet = planet_res.instance()
				var planet_root = Node2D.new()
				add_child(planet_root)
				planet_root.set_owner(self)
				planet_root.rotate(i["rot"])
				planet_root.position = center
				planet.position.y = dis
				planet.rotation = rand_range(0,360)
				planet_root.add_child(planet)
				planet.recreate(i)
				planet.set_owner(planet_root)
				p_roots.append(planet_root)
				planet_num += 1
	emit_signal("finished_naming")
	var nnum = 0
	for i in nebs:
		i.modulate = data["neb_cols"][nnum]
		nnum += 1

func generate():
	randomize()
	var cc = rand_range(0,1)
	if cc > 0.8:
		$TextureButton.modulate = colours[randi() % colours.size()]
	var sca = rand_range(250,500)
	center = Vector2(250,250) * sca
	$TextureButton.rect_scale = Vector2(sca,sca)
	var planets = 1 + randi() % 10
	var dis = rand_range(-350000,-675000)
	if planets > 0:
		for i in planets:
			var ast_c = rand_range(0,1)
			if ast_c < 0.3:
				var ast_root = Node2D.new()
				add_child(ast_root)
				ast_root.set_owner(self)
				ast_root.position = center
				ast_root.set_script(preload("res://scripts/ast_root.gd"))
				ast_root.mass = dis * -0.000000001
				ast_root.dis = dis
				var ast_pivot = Node2D.new()
				ast_root.add_child(ast_pivot)
				ast_pivot.name = 'pivot'
				create_asteroid_belt(Vector2(0,0),dis,0,360,ast_pivot)
				p_roots.append(ast_root)
				belts.append(ast_root)
			else:
				var planet = planet_res.instance()
				var planet_root = Node2D.new()
				add_child(planet_root)
				planet_root.set_owner(self)
				planet_root.rotate(rand_range(0,100))
				planet_root.position = center
				planet.position.y = dis
				planet.rotation = rand_range(0,360)
				planet_root.add_child(planet)
				planet.set_owner(planet_root)
				p_roots.append(planet_root)
				planet_num += 1
				planet.generate()
				planet_nodes.append(planet)
			distances.append(dis)
			dis -= rand_range(150000,250000)
	
	for i in nebs:
		i.modulate = nebcols[randi() % nebcols.size()]

func create_asteroid_belt(center, radius, angle_from, angle_to, root):
	var num : int  = floor((radius/500)*-1)
	var nb_points : int = num + randi() % int(floor(num * 1.8))
	var points_arc = PoolVector2Array()

	for i in range(nb_points + 1):
		var angle_point = deg2rad(angle_from + i * (angle_to-angle_from) / nb_points - 90)
		points_arc.push_back(center + Vector2(cos(angle_point), sin(angle_point)) * radius)

	for index_point in range(nb_points):
		var c = rand_range(0.00,1.00)
		var ast = dud_asteroid_res.instance()
		var offset = Vector2(rand_range(-num*7,num*7),rand_range(-num*7,num*7))
		var sca = rand_range(0.0001,20.00)
		ast.scale = Vector2(sca,sca)
		ast.rotation = rand_range(0,360)
		ast.position = points_arc[index_point]
		ast.position += offset
		root.add_child(ast)
		ast.set_owner(root)

func _process(delta):
	if !name_chosen:
		get_parent().try_name(self)
	else:
		if naming:
			name = star_name
			var num = 1
			for i in planet_nodes:
				var pname = (star_name + ' ' + str(num))
				i.name = pname
				num += 1
				for o in i.m_roots:
					var moon = o.get_child(0)
					var mnum = moon.num
					var mname = (pname + ' ' + letters[mnum])
					moon.name = mname
			naming = false
			emit_signal('finished_naming')
			game.create_planet_icons()
			if stats == null:
				save_data()
	
	for i in planet_nodes:
		var r = i.get_parent()
		r.rotate((i.mass * 0.0000015) * delta)
	for i in belts:
		i.rotate(i.mass * 0.003)

func save_data():
	var bodies = []
	var dis
	var num = 0
	var nc = []
	for i in game.nebs:
		nc.append(i.modulate)
	var list = planet_nodes + belts
	for i in list:
		match i.type:
			"planet":
				bodies.append(i.save_stats())
			"ast":
				var data = {
					body_type = "asteroid",
					distance = i.dis
					}
				bodies.append(data)
		num += 1
		
	stats = {
			sname = star_name,
			size = $TextureButton.rect_scale.x,
			colour = $TextureButton.modulate,
			bodies = bodies,
			neb_cols = nc,
			player = {
				pos = game.player.global_position,
				wep = game.player.weapon
				}
			}
	return stats

func _on_TextureButton_mouse_entered():
	game.update_label(self)

func _on_TextureButton_mouse_exited():
	game.hide_label()
