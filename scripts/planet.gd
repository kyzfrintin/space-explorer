extends Node2D

var distance = 0
var mass = 100

var m_roots = []
var sats = []
var stations = []
var occupied = false
var type = "planet"
var center = Vector2(0,0)
var clouds = []
var colours = []
var planet_stats = {}
var created_defences = false
var on_screen = false
var showing = false
var palette = [Color(0.0562, 0.0258, 0.4140),Color(0.3803,0,0.0352),Color(0.0156,0.4627,0.4627),
Color(0.4274,0.0156,0.4627),Color(0.0705,0.4235,0.0117),Color(0.4117,0.4235,0.0117),
Color(0.482353, 0.1137, 0.6862),Color(0.871094, 0.2041, 0.3135),Color(0.898438, 0.4888, 0.2667)]
onready var sun = get_node("../../")
onready var game = sun.get_node("../../")
onready var sprites = [get_node("TextureButton"),get_node("Sprite"),get_node("Sprite2")]
onready var moon_res = preload("res://scenes/moon.tscn")
onready var ss_res = preload("res://scenes/space_station.tscn")
onready var plat_res = preload("res://scenes/defence_platform.tscn")
onready var mine_res = preload("res://scenes/space_mine.tscn")
onready var cloud_tex = [preload("res://assets/art/clouds.png"),preload("res://assets/art/clouds2.png"),
preload("res://assets/art/clouds3.png")]
onready var tex = [[preload("res://assets/art/planet_white.png"),preload("res://assets/art/planet_black.png")],
[preload("res://assets/art/planet_white2.png"),preload("res://assets/art/planet_black2.png")],
[preload("res://assets/art/planet_white3.png"),preload("res://assets/art/planet_black3.png")]]

func recreate(data):
	var sca = data["size"]
	var textures = data["textures"]
	var cols = data["planet_colours"]
	var cs = data["clouds"]
	var moons = data["moons"]
	colours = cols
	$TextureButton.texture_normal = textures[0]
	$Sprite.texture = textures[1]
	name = data["planet_name"]
	center = Vector2(2000,2000) * sca
	$TextureButton.rect_scale = Vector2(sca,sca)
	$TextureButton.rect_position = -center
	$Sprite.scale = Vector2(sca,sca)
	$Sprite2.scale = Vector2(sca,sca)
	occupied = data["settled"]
	mass = 80*sca
	$TextureButton.modulate = cols[0]
	$Sprite.modulate = cols[1]
	for i in cs:
		add_child(i)
	move_child($Sprite2,get_child_count())
	if moons.size() > 0:
		var ss_num = 1
		var moon_num = 0
		for i in moons:
			var moon
			if i["body_type"] == "space_station":
				moon = ss_res.instance()
			else:
				moon = moon_res.instance()
				moon_num += 1
			var moon_root = Node2D.new()
			add_child(moon_root)
			moon_root.set_owner(self)
			moon.position.y = i["distance"]
			moon.rotation = rand_range(0,360)
			moon_root.rotate(rand_range(0,100))
			moon_root.add_child(moon)
			moon.recreate(i)
			moon.set_owner(moon_root)
			if moon.type == "space_station":
				moon.num = ss_num
				ss_num += 1
			m_roots.append(moon_root)

func generate():
	randomize()
	var itex = randi() % tex.size()
	$TextureButton.texture_normal = tex[itex][0]
	$Sprite.texture = tex[itex][1]
	var sca = rand_range(3.0,8.0)
	center = Vector2(2000,2000) * sca
	$TextureButton.rect_scale = Vector2(sca,sca)
	$TextureButton.rect_position = -center
	$Sprite.scale = Vector2(sca,sca)
	$Sprite2.scale = Vector2(sca,sca)
	if rand_range(0,1) > 0.2:
		occupied = true
	mass = 80*sca
	recolour()
	generate_clouds(sca)
	move_child($Sprite2,get_child_count())
	var moons = 1 + randi() % 5
	var dis = rand_range(-35500,-37250)
	if moons > 0:
		var ss_num = 1
		var moon_num = 0
		for i in moons:
			var ss_c = rand_range(0,1)
			var moon
			if ss_c < 0.4 and occupied:
				moon = ss_res.instance()
			else:
				moon = moon_res.instance()
			var moon_root = Node2D.new()
			add_child(moon_root)
			moon_root.set_owner(self)
			moon.position.y = dis
			moon.rotation = rand_range(0,360)
			moon_root.rotate(rand_range(0,100))
			moon_root.add_child(moon)
			moon.generate()
			moon.set_owner(moon_root)
			dis -= rand_range(3000,7500)
			match moon.type:
				"space_station":
					moon.num = ss_num
					ss_num += 1
				"moon":
					moon.num = moon_num
					moon_num += 1
					
			m_roots.append(moon_root)
	
func save_stats():
	var cs = []
	for i in clouds:
		var cloud_data = {
			tex = i.texture,
			size = i.scale.x,
			colour = i.modulate
			}
	var ms = []
	for i in m_roots:
		var m = i.get_child(0)
		ms.append(m.save_stats())
	planet_stats = {
		body_type = "planet",
		planet_name = name,
		settled = occupied,
		planet_colours = colours,
		size = $TextureButton.rect_scale.x,
		clouds = cs,
		moons = ms,
		textures = [$TextureButton.texture_normal, $Sprite.texture],
		distance = position.y,
		rot = get_parent().rotation
		}
	return planet_stats

func generate_clouds(sca):
	var c = randi() % 6
	var csca = 1.008
	for i in c:
		var cloud = Sprite.new()
		cloud.texture = cloud_tex[randi() % cloud_tex.size()]
		cloud.scale = Vector2(sca * csca, sca * csca)
		cloud.rotate(rand_range(0,360))
		csca += rand_range(0.006,0.012)
		add_child(cloud)
		cloud.set_owner(self)
		var shader = preload("res://assets/shaders/nebula_ripple.shader")
		var nmat = ShaderMaterial.new()
		cloud.set_material(nmat)
		var mat = cloud.get_material()
		mat.shader = shader
		mat.set_shader_param("speed", csca * 0.02)
		mat.set_shader_param("volume", csca * rand_range(0.5,1.2))
		mat.set_shader_param("volume1", csca * rand_range(0.5,1.2))
		mat.set_shader_param("volume2", csca * rand_range(0.5,1.2))
		mat.set_shader_param("volume3", csca * rand_range(0.5,1.2))
		clouds.append(cloud)
		sprites.append(cloud)
		cloud.modulate = Color(rand_range(0.7,1),rand_range(0.7,1),rand_range(0.7,1),rand_range(0.02,0.30))

func generate_platforms():
	var plats = floor($TextureButton.rect_scale.x * rand_range(0.6,2.75))
	for i in plats:
		var plat = plat_res.instance()
		var pos = Vector2(rand_range(-12000,12000),rand_range(-12000,12000))
		plat.position = pos
		plat.add_to_group("enemies")
		add_child(plat)

func generate_mines(num):
	num *= rand_range(0.7,1.3)
	var mines = floor(num*rand_range(6,10))
	for i in mines:
		var mine = mine_res.instance()
		var off = Vector2(rand_range(-12000,12000),rand_range(-12000,12000))
		mine.parent_planet = self
		mine.offset = off
		mine.position = global_position + off
		mine.add_to_group("enemies")
		game.get_node("enemies").add_child(mine)

func recolour():
	randomize()
	var col1 = Color(rand_range(0,1),rand_range(0,1),rand_range(0,1),1)
	var col2 = Color(rand_range(0,1),rand_range(0,1),rand_range(0,1),1)
#	var col1 = palette[randi() % palette.size()]
#	var newpal = palette
#	newpal.remove(newpal.find(col1))
#	var col2 = newpal[randi() % newpal.size()]
	colours = [col1,col2]
	$TextureButton.modulate = col1
	$Sprite.modulate = col2

func _process(delta):
	$Sprite2.look_at(sun.global_position)
	if on_screen:
		if !showing:
			toggle_planet(true)
		for i in m_roots:
			var m = i.get_child(0)
			i.rotate((m.mass * 0.002) * delta)
		if clouds.size() > 0:
			for i in clouds:
				i.rotate(0.0005 / i.scale.x)
		if global_position.distance_to(game.get_node("player").position) < 75000:
			if occupied and !created_defences:
				created_defences = true
				generate_platforms()
				generate_mines($TextureButton.rect_scale.x)
	else:
		if showing:
			toggle_planet(false)

func toggle_planet(tog):
	if tog:
		for i in sprites:
			i.show()
			showing = true
	else:
		for i in sprites:
			if global_position.distance_to(game.get_node("player").position) > 75000:
				i.hide()
				showing = false

func _on_TextureButton_mouse_entered():
	game.update_label(self)

func _on_TextureButton_mouse_exited():
	game.hide_label()

func enter_screen():
	on_screen = true

func exit_screen():
	on_screen = false
