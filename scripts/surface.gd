extends Node2D

var planet_name
var surface_stats : Dictionary = {}
var shake
var shake_num = 0
var shake_amnt = 40

onready var player = get_node("ship")
onready var p_cam = player.cam

func _ready():
	grab_world_data()
	$UI/AnimationPlayer.play("name_fade")
	store_world_data()
	scene_holder.surface_loaded(self)

func _process(delta):
	if Input.is_action_just_pressed("ui_rightclick"):
		scene_holder.liftoff(self)
	
	if shake && shake_num > 0:
		shake_num -= 1
		p_cam.offset = Vector2(rand_range(-shake_amnt, shake_amnt), rand_range(-shake_amnt, shake_amnt))
	else:
		p_cam.offset = Vector2(0,0)
	
	if player.position.x > 100000:
		player.global_position.x = -99900
		print('from right to left')
	elif player.position.x < -100000:
		player.global_position.x = 99900
		print('from left to right')
	if player.position.y > 100000:
		player.global_position.y = -99900
		print('from bottom to top')
	elif player.position.y < -100000:
		player.global_position.y = 99900
		print('from top to bottom')

func store_world_data():
	scene_holder.system_surfaces[planet_name] = self
	
func grab_world_data():
	var data = scene_holder.surface_stats
	planet_name = data['planet_name']
	$white.modulate = data['planet_colours'][0]
	$black.modulate = data['planet_colours'][1]
	$UI/Label.text = planet_name
	
func zoom(z):
	pass

func shake(intensity):
	shake = true
	shake_num = 20
	shake_amnt = intensity
