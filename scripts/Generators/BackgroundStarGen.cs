using Godot;
using System;

class BackgroundStarGen : Node2D {
  [Export]
  public int backgroundStarCount = 1;
  [Export]
  public float range = 200000;
  [Export]
  public int seed;
  private Texture starTexture;
  private static Color[] colors = new Color[2] {
    new Color(1,0,0),
    new Color(0,0,1)
  };

  public override void _Ready() {
    Random random = new Random(seed);
    starTexture = GD.Load("res://assets/art/sun.png") as Texture;
    for (int i = 0; i < backgroundStarCount; i++) PlaceBackgroundStar();
  }

  private void PlaceBackgroundStar() {
    Sprite star = new Sprite();
    star.Modulate = new Color(1f, 1f, 0.7843f);
    star.Texture = starTexture;
    Random rand = new Random(seed);
    float scale = (float)rand.NextDouble() * (0.3f - 0.01f) + 0.01f;
    star.Scale = new Vector2(scale, scale);
    float colorNum = (float)rand.NextDouble();
    if (colorNum > 0.8f) 
      star.Modulate = colors[rand.Next(0, colors.Length - 1)];
    float posX = (float)rand.NextDouble() * (range*2) - range;
    float posY = (float)rand.NextDouble() * (range*2) - range;
    star.Position = new Vector2(posX, posY);
    AddChild(star);
    star.Owner = this;
  }
}
