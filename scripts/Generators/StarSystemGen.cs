using Godot;
using System;
using System.Collections.Generic;

public class StarGen : Node2D {
  [Export]
  public int seed;
  public Dictionary<int, float> planetWeights = new Dictionary<int, float>() {
    {0, 0.01f},
    {1, 0.15f},
    {2, 0.2f},
    {3, 0.25f},
    {4, 0.3f},
    {5, 0.25f},
    {6, 0.225f},
    {7, 0.215f},
    {8, 0.175f},
    {9, 0.15f},
    {10, 0.1f}
  };
  [Export]
  /// <summary>
  /// The range that star systems are allowed to place planets.
  /// </summary>
  public float systemRange = 20000;

  private static Color[] colors = new Color[2] {
    new Color(1,0,0),
    new Color(0,0,1)
  };

  PackedScene starScene;
  PackedScene planetScene;
  PackedScene asteroidScene;
  PackedScene dudAsteroidScene;
  Random random;

  public override void _Ready() {
    starScene = GD.Load("res://scenes/sun.tscn") as PackedScene;
    planetScene = GD.Load("res://scenes/planet.tscn") as PackedScene;
    asteroidScene = GD.Load("res://scenes/crate.tscn") as PackedScene;
    dudAsteroidScene = GD.Load("res://scenes/dud_asteroid.tscn") as PackedScene;
    random = new Random(seed);
    GenerateSystem();
  }

  /// <summary>
  /// This generates a star system's position, can calls children that
  /// generate the rest of the system.
  /// </summary>
  private void GenerateSystem() {
    Node2D centerStar = PlaceStar();
    WeightedSelection<int> weightedSelection = new WeightedSelection<int>(
      random.Next(Int32.MinValue, Int32.MaxValue),
      planetWeights
    );
    int planetCount = weightedSelection.Next();
    for (int i = 0; i < planetCount; i++) {
      GeneratePlanet(centerStar);
    }
  }

  private Planet GeneratePlanet(Node2D star) {
    Planet planet = planetScene.Instance() as Planet;
    star.AddChild(planet);
    planet.Owner = star;
    planet.Rotate(((float)random.NextDouble() * 200) - 100);
    planet.seed = random.Next(Int32.MinValue, Int32.MaxValue);
    planet.Generate();
    return planet;
  }

  private Node2D PlaceStar() {
    Node2D star = starScene.Instance() as Node2D;
    AddChild(star);
    star.Owner = this;
    return star;
  }
}