extends Node2D

var active = false
var target = null
var speed = 1
var exploded = false
var beep_cd = 0.8
var parent_planet
var offset

onready var game = get_node("../..")

func _ready():
	$blinktimer.start(rand_range(1,4))

func blink():
	$blink.visible = true
	if !active:
		$blinktimer.start(1)
		yield(get_tree().create_timer(0.1), "timeout")
		$blink.visible = false

func _process(delta):
	var spos = global_position
	if active:
		var ppos = target.position
		var dis = spos.distance_to(ppos)
		if speed < 50:
			speed *= 1.05
		if dis < 250 and !exploded:
			boom()
		else:
			position += spos.direction_to(ppos)*speed
	else:
		if spos.distance_to(game.get_node("player").global_position) < 10000:
			if !$activate.monitoring:
				$activate.monitoring = true
		else:
			if $activate.monitoring:
				$activate.monitoring = false
		var c = parent_planet.global_position
		position = c + offset

func boom():
	$explode_timer.stop()
	exploded = true
	var victims = $damage.get_overlapping_bodies()
	if victims.size() > 0:
		for i in victims:
			if i.name == "player":
				i.hit(30,position)
	$boom.emitting = true
	$body.queue_free()
	$blink.queue_free()
	$blinktimer.stop()
	$beep_timer.stop()
	$splode.play()
	$boom_timeout.start()
	
func activate(body):
	if !active:
		if body.name == "player":
			$damage.monitoring = true
			$blink.visible = true
			$activate.queue_free()
			$explode_timer.start()
			beep()
			active = true
			target = body

func remove():
	queue_free()

func beep():
	$beep.play()
	beep_cd *= 0.9
	$beep_timer.start(beep_cd)
