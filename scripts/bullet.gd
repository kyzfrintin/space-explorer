extends Node2D

var dir = Vector2(0,0)
var speed = 130
var damage
var friendly : bool

onready var game = get_node("../../")
onready var ding_res = preload("res://scenes/ding.tscn")

func _process(delta):
	position += dir*speed

func _enter_tree():
	$shot.spawn_node = get_node("../../sounds")
	
func _enable():
	$Area2D.monitoring = true

func on_hit(body):
	collide(body)

func collide(victim):
	if victim.name == "player" and friendly: return
	elif !friendly and victim.get_parent().is_in_group("enemies"): return
	call_deferred("spawn_ding")
	if victim.has_method("hit"):
		victim.hit(damage * (rand_range(0.8,1.2)), position)
		queue_free()

func spawn_ding():
	var ding = ding_res.instance()
	game.fx.add_child(ding)
	ding.position = global_position
	ding.scale = Vector2(13,13)

func _area_hit(area):
	collide(area)

func _on_Timer_timeout():
	queue_free()


