extends Node2D

var dir = Vector2(0,0)
var speed = 50
var damage
var target : Vector2
var friendly : bool

onready var game = get_node("../../")
onready var splode_res = preload("res://scenes/missile_explode.tscn")

func _process(delta):
	position += dir*speed
	if position.distance_to(target) < 50:
		explode()

func _enter_tree():
	$boom.spawn_node = get_node("../../sounds")
	
func _ready():
	look_at(target)

func explode():
	$boom.play()
	var splode = splode_res.instance()
	splode.position = global_position
	splode.damage = damage
	splode.friendly = friendly
	game.get_node("fx").add_child(splode)
	queue_free()

func collide(victim):
	if victim.name == "player" and friendly: return
	elif !friendly and victim.get_parent().is_in_group("enemies"): return
	if victim.has_method("hit"):
		explode()
	
func on_hit(body):
	collide(body)

func _on_Timer_timeout():
	explode()

func _on_Area2D_area_entered(area):
	collide(area)

func enable():
	$Area2D.monitoring = true
