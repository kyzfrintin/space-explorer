extends KinematicBody2D

export var thrust = 300
export(float) var zoom_factor = 5
export(float) var damp = 1
export(float) var max_speed = 50000
export(float, 1, 150, 1.0) var max_zoom = 150
var zf

var x_axis
var y_axis
var vel = Vector2()
var pos = Vector2()
var acc = Vector2()
var processing_input = true
var autopiloting
var dest
var can_fire = true
var can_launch = true
var weapons = ["Laser", "Missile"]
var weapon
var damping = false
var HP = 100.0
var healing = false

onready var bullet_res = preload("res://scenes/bullet.tscn")
onready var missile_res = preload("res://scenes/missile.tscn")
onready var cast = get_node("RayCast2D")
onready var tween = get_node("Tween")
onready var cam = get_node("Camera2D")
onready var level = get_parent()

func _ready():
	reset_cam()
	weapon = weapons[0]
	zf = zoom_factor
	$RayCast2D.cast_to.x = thrust*5

func _handle_movement_input() -> Vector2:
	var accel : Vector2
	x_axis = Input.get_joy_axis(0,0)
	y_axis = Input.get_joy_axis(0,1)
	
	if Input.is_action_pressed("gp_left"):
			x_axis = -1
	if Input.is_action_pressed("gp_right"):
			x_axis = 1
	if Input.is_action_pressed("gp_up"):
			y_axis = -1
	if Input.is_action_pressed("gp_down"):
			y_axis = 1
	look_at(Vector2(position.x + x_axis,position.y + y_axis))
	if x_axis != 0 or y_axis != 0:
		accel = Vector2(thrust, 0).rotated(rotation)
	else:
		accel = Vector2(0,0)
	
	if Input.is_action_just_pressed("gp_damper"):
		if damping:
			damping = false
			$dampers_off.play()
		else:
			damping = true
			$dampers_on.play()
	
	if Input.is_action_just_pressed("ui_accept"):
		tween.interpolate_property(self, 'vel', vel, cast.cast_to.rotated(rotation)*2, 1.6, Tween.TRANS_BACK, Tween.EASE_OUT)
		tween.start()
		$boost.play()
		level.shake(30)

	return accel

func _handle_attack_input():
	var mpos = get_global_mouse_position()
	if Input.is_action_pressed("gp_fire"):
		match weapon:
			"Laser":
				if can_fire:
					var bullet = bullet_res.instance()
					bullet.dir = position.direction_to(mpos)
					bullet.position = global_position
					bullet.friendly = true
					bullet.damage = 25
					level.bullets.add_child(bullet)
					can_fire = false
					$attack_timer.start()
					level.shake(10)
			"Missile":
				if can_launch:
					var missile = missile_res.instance()
					missile.dir = position.direction_to(mpos)
					missile.target = get_global_mouse_position()
					missile.position = global_position
					missile.friendly = true
					missile.damage = 125.0
					level.bullets.add_child(missile)
					can_launch = false
					$missile_timer.start()
					level.shake(10)

	var w : int = weapons.find(weapon)
	
	if Input.is_action_just_pressed("gp_prev_wep"):
		w -= 1
		w = clamp(w, 0, weapons.size() - 1)
		weapon = weapons[w]
		level.wep_label.text = weapon
	if Input.is_action_just_pressed("gp_next_wep"):
		w += 1
		w = clamp(w, 0, weapons.size() - 1)
		weapon = weapons[w]
		level.wep_label.text = weapon

func _handle_camera_input():
	
	var z : float = cam.zoom.x
	
	if Input.is_action_pressed("ui_zoom_in"):
		z -= zoom_factor
		zoom_factor *= 1.01
		level.zoom(z)
	if Input.is_action_pressed("ui_zoom_out"):
		z += zoom_factor
		zoom_factor *= 1.01
		level.zoom(z)
	if Input.is_action_just_released("ui_zoom_in") or Input.is_action_just_released("ui_zoom_out"):
		zoom_factor = zf
	
	if Input.is_action_just_pressed("ui_zoom_reset"):
		reset_cam()

	return z

func reset_cam():
	cam.zoom = Vector2(1,1)
	level.zoom(1)

func autopilot(loc):
	
	look_at(loc)
	var look_ahead = vel.length()
	var stop_dis
	match typeof(dest):
		TYPE_VECTOR2:
			stop_dis = 500
		TYPE_OBJECT:
			var sca = dest.get_node("TextureButton").rect_scale.x
			stop_dis = 2000 * sca
	if global_position.distance_to(loc) > (stop_dis + look_ahead):
		var vec = global_position.direction_to(loc)
		damping = false
		return vec*thrust
	else:
		level.get_node("auto_off").play()
		autopiloting = false
		dest = null
		damping = true
		return Vector2(0,0)

func hit(damage, pos):
	HP -= damage
	update_hp("hit")
	healing = false
	$heal_timer.start()

func update_hp(type):
	var bar = level.get_node("UI/healthlabel/hp")
	var tween : Tween = bar.get_node("Tween")
	bar.value = HP
	var a = (HP/100) * -1
	a = clamp(a+1,0,1)
	level.get_node("UI/healthlabel/red_edges").modulate = Color(1,1,1,a)
	match type:
		"hit":
			tween.interpolate_property(bar, 'modulate', Color(1,0,0,1), Color(1,1,1,1), 0.7, Tween.TRANS_LINEAR, Tween.EASE_IN)
			tween.start()
		"heal":
			if HP != 100:
				if !(bar.modulate == Color(0,0.8,0,1)):
					bar.modulate = Color(0,0.8,0,1)
			else:
				if !(bar.modulate == Color(1,1,1,1)):
					bar.modulate = Color(1,1,1,1)

func move_to(vec, delta):
	var new_vel = (vel + (vec * delta))
	if new_vel.length() < max_speed:
		vel += vec * delta
	
	if vec == Vector2(0,0):
		if damping:
			vel = lerp(vel,Vector2(0,0),damp)
		if $thrust_loop.playing:
			$thrust_loop.stop()
			$thrust_off.stop()
			$Particles2D.emitting = false
	else:
		if !$thrust_loop.playing:
			$thrust_loop.play()
			$thrust_off.play()
			$Particles2D.emitting = true
	
	pos += vel
	position = pos


func _process(delta):
	acc = Vector2(0,0)
	if processing_input:
		
		var accel = _handle_movement_input()
		if accel != Vector2(0,0):
			acc = accel
			if autopiloting:
				autopiloting = false
				dest = null

		var z = _handle_camera_input()
		
		z = clamp(z, 0.4, max_zoom)
		cam.zoom = Vector2(z,z)
		
		_handle_attack_input()
	if autopiloting:
		if dest != null:
			match typeof(dest):
				TYPE_STRING:
					pass
				TYPE_OBJECT:
					acc = autopilot(dest.global_position + dest.center)
		
	move_to(acc, delta)
	if healing and HP < 100.0:
		HP += 1
		HP = clamp(HP,0,100)
		update_hp("heal")

func reload():
	can_fire = true

func load_missile():
	can_launch = true

func start_healing():
	healing = true
