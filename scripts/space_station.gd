extends Node2D

var distance = 0
var mass = 100
var type = "space_station"
var num = 0
var center = Vector2(0,0)
var moon_stats
onready var planet = get_node("../../")
onready var sun = planet.get_node("../../")
onready var game = sun.get_node("../../")

func generate():
	var sca = rand_range(1.3,3.7)
	center = Vector2(250,250) * sca
	scale = Vector2(sca,sca)
	mass = 10*sca

func recreate(data):
	var sca = data["size"]
	center = Vector2(250,250) * sca
	scale = Vector2(sca,sca)
	name = data["mname"]
	mass = 10*sca
	pass

func save_stats():
	moon_stats = {
		body_type = "space_station",
		mname = name,
		size = $TextureButton.rect_scale.x,
		distance = position.y,
		rot = get_parent().rotation
		}
	return moon_stats

func _on_TextureButton_mouse_entered():
	game.update_label(self)

func _on_TextureButton_mouse_exited():
	game.hide_label()
