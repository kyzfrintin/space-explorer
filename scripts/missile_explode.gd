extends Node2D

var damage : float
var friendly : bool

func _on_Timer_timeout():
	queue_free()

func on_touch(body):
	collide(body)

func collide(victim):
	if victim.name == "player" and friendly: return
	elif !friendly and victim.get_parent().is_in_group("enemies"): return
	if victim.has_method("hit"):
		var d = damage * rand_range(0.8,1.2)
		victim.hit(d,position)

func _ready():
	$boom.emitting = true
	yield(get_tree().create_timer(0.02),"timeout")
	$Area2D.queue_free()


func _on_Area2D_area_entered(area):
	collide(area)
