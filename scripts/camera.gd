extends Camera2D

export(int) var zoom_factor = 5

onready var game = get_parent()
onready var player = get_node("../player")
onready var bgs = [get_node("../bg_parallax/nebula_far/1"),get_node("../bg_parallax/nebula_far/2"),
get_node("../bg_parallax/nebula_mid/2"),get_node("../bg_parallax/nebula_mid/3"),
get_node("../bg_parallax/nebula_close/3"),get_node("../bg_parallax/nebula_close/4"),
get_node("../bg_parallax/ripples_far/Sprite1"),get_node("../bg_parallax/ripples_close/Sprite2")]
var zf
var speed = 10
var s

func _ready():
	zf = zoom_factor
	s = speed
	reset()

func _input(event):
	if event is InputEventMouseMotion:
		if Input.is_mouse_button_pressed(1):
			position -= (event.relative * 3) * zoom.x

func reset():
	game.zoom(1000)
	zoom = Vector2(1000,1000)
	position = get_node("../player").position

func _process(delta):
	var z = zoom.x
	if Input.is_action_pressed("ui_zoom_in"):
		z -= zoom_factor
		if zoom_factor < 40:
			zoom_factor *= 1.01
		game.zoom(z)
	if Input.is_action_pressed("ui_zoom_out"):
		z += zoom_factor
		if zoom_factor < 40:
			zoom_factor *= 1.01
		game.zoom(z)
	if Input.is_action_just_released("ui_zoom_in") or Input.is_action_just_released("ui_zoom_out"):
			zoom_factor = zf
	
	if Input.is_action_just_pressed("ui_zoom_reset"):
			z = 1000
			position = get_node("../player").position

	z = clamp(z,20,3000)
	zoom = Vector2(z,z)
	
	var move = Vector2(0,0)
	
	if Input.is_action_pressed("gp_up"):
		speed *= 1.01
		move.y -= speed * z
	if Input.is_action_pressed("gp_down"):
		speed *= 1.01
		move.y += speed * z
	if Input.is_action_pressed("gp_left"):
		speed *= 1.01
		move.x -= speed * z
	if Input.is_action_pressed("gp_right"):
		speed *= 1.01
		move.x += speed * z
	
	if move == Vector2(0,0):
		speed = s
	
	position += move
	
	if Input.is_action_just_pressed("ui_open_map"):
		if get_parent().map_open:
			close_map()
		else:
			open_map()
			
func open_map():
	for i in bgs:
		i.scale = Vector2(5500,5500)
	current = true
	player.cam.current = false
	player.processing_input = false
	reset()
	get_parent().map_open = true
	get_node("../UI/icons/player_indicator").visible = true

func close_map():
	for i in bgs:
		i.scale = Vector2(1000,1000)
	current = false
	player.cam.current = true
	player.processing_input = true
	get_parent().map_open = false
	game.zoom(player.cam.zoom.x)
	get_node("../UI/icons/player_indicator").visible = false
