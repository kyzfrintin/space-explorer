extends KinematicBody2D

export var thrust = 300
export var max_spd = 6
export var turn_spd = 2.6
export(float) var zoom_factor = 5
export(float) var damp = 1

var zf

onready var emitters = [get_node("smoke"), get_node("fire")]
onready var view_size = get_viewport_rect().size
onready var tween = get_node("Tween")
onready var level = get_parent()
onready var cam = get_node("Camera2D")
onready var cast = get_node("RayCast2D")
onready var bullet_res = load("res://scenes/p_laser.tscn")
onready var boostemit = get_node("boost")
onready var thrust_start = get_node("sounds/thrust_off")
onready var thrust_loop = get_node("sounds/move_loop")
onready var thrust_rumble = get_node("sounds/thrust_rumble")
onready var boost_sound = get_node("sounds/boost")
onready var laser_sound = get_node("sounds/laser")
onready var scrape_sound = get_node("sounds/ship_scrape")
onready var hit_sound = get_node("sounds/ship_hit")

var x_axis
var y_axis
var shake = false
var shake_num = 0
var shake_amnt = 40
var vel = Vector2()
var rot = 0
var pos = Vector2()
var acc = Vector2()
var canfire = true
var HP = 100
var MaxHP = 100
var las_dam = 25
var cannons = 1
var bullets = []
var laser_speed = 4500
var las_mult = 1
var vul = true

signal death
signal hurt

func _ready():
#	MaxHP = CustCarrier.MaxHP
#	HP = CustCarrier.HP
#	las_mult = CustCarrier.las_mult
	zf = zoom_factor
	$RayCast2D.cast_to.x = thrust*5
	connect("death", get_parent(), "player_died")
	connect("hurt", get_parent(), "player_hit")
	
func _process(delta):
	if !level.map_open:
		if Input.is_action_just_pressed("cheat_god_mode"):
			if vul:
				vul = false
			else:
				vul = true
		
		x_axis = Input.get_joy_axis(0,0)
		y_axis = Input.get_joy_axis(0,1)
		
		if Input.is_action_pressed("gp_left"):
				x_axis = -1
		if Input.is_action_pressed("gp_right"):
				x_axis = 1
		if Input.is_action_pressed("gp_up"):
				y_axis = -1
		if Input.is_action_pressed("gp_down"):
				y_axis = 1
		
		var z : float = $Camera2D.zoom.x
		
		if Input.is_action_pressed("ui_zoom_in"):
			z -= zoom_factor
			zoom_factor *= 1.01
		if Input.is_action_pressed("ui_zoom_out"):
			z += zoom_factor
			zoom_factor *= 1.01
		
		if Input.is_action_just_released("ui_zoom_in") or Input.is_action_just_released("ui_zoom_out"):
			zoom_factor = zf
		
		if Input.is_action_just_pressed("ui_zoom_reset"):
			z = 1
		
		z = clamp(z, 0.4, 100)
		$Camera2D.zoom = Vector2(z,z)
		
		var look_x = position.x + x_axis
		var look_y = position.y + y_axis
		var look = Vector2(look_x,look_y)
		look_at(look)
		
		
		if x_axis != 0 or y_axis != 0:
			acc = Vector2(thrust, 0).rotated(rotation)
			for i in emitters:
				i.emitting = true
			
			if !thrust_start.playing:
				thrust_start.play()
			if !thrust_loop.playing:
				thrust_loop.play()
			if !thrust_rumble.playing:
				thrust_rumble.play()
		else:
			acc = Vector2(0,0)
			vel = lerp(vel,Vector2(0,0),damp)
			for i in emitters:
				i.emitting = false
			if thrust_start.playing:
				thrust_start.stop()
			if thrust_loop.playing:
				thrust_loop.stop()
	
		if Input.is_action_just_pressed("ui_accept"):
			tween.interpolate_property(self, 'vel', vel, cast.cast_to.rotated(rotation)*5, 1.6, Tween.TRANS_BACK, Tween.EASE_OUT)
			tween.start()
			boost_sound.play()
			boostemit.emitting = true
			shake(30)
		
#		if Input.is_mouse_button_pressed(1) && canfire:
#			for i in range(0, cannons):
#				var laser = bullet_res.instance()
#				laser.damage = (las_dam*las_mult)*(rand_range(0.5*las_mult,2.5*las_mult))
#				laser.speed = laser_speed*las_mult
#				laser.scale += Vector2(las_mult/3,las_mult/3)
#				laser.position = position
#				laser.modulate.a = 0
#				if las_mult > 1:
#					laser.modulate.g -= las_mult/10
#					laser.modulate.r -= las_mult/5
#					laser.modulate.b += las_mult/12
#				laser.targetx = get_global_mouse_position().x
#				laser.targety = get_global_mouse_position().y
#				get_parent().get_node("bullets").add_child(laser,true)
#				bullets.append(laser)
#			if bullets.size() == 2:
#				bullets[0].position += Vector2(0, -20)
#				bullets[1].position += Vector2(0, 20)
#			if bullets.size() == 3:
#				bullets[1].position += Vector2(0, -30)
#				bullets[2].position += Vector2(0, 30)
#			bullets.clear()
#			canfire = false
#			get_node("Timer").wait_time = 1/(las_mult*7.5)
#			get_node("Timer").start()
#			laser_sound.play()
#			shake(15)
#
#		if Input.is_action_pressed("gp_pad_fire") && canfire:
#			for i in range(0, cannons):
#				var laser = bullet_res.instance()
#				laser.damage = (las_dam*las_mult)*(rand_range(0.5*las_mult,2.5*las_mult))
#				laser.speed = laser_speed*las_mult
#				laser.position = position
#				laser.scale *= las_mult
#				laser.modulate.a = 0
#				if las_mult > 1:
#					laser.modulate.g -= las_mult/10
#					laser.modulate.r -= las_mult/5
#					laser.modulate.b += las_mult/12
#				laser.targetx = (position.x + (Input.get_joy_axis(0, 2))*4)
#				laser.targety = (position.y + (Input.get_joy_axis(0, 3))*4)
#				get_parent().get_node("bullets").add_child(laser)
#				bullets.append(laser)
#			if bullets.size() == 2:
#				bullets[0].position += Vector2(0, -20)
#				bullets[1].position += Vector2(0, 20)
#			if bullets.size() == 3:
#				bullets[1].position += Vector2(0, -30)
#				bullets[2].position += Vector2(0, 30)
#			bullets.clear()
#			canfire = false
#			get_node("Timer").wait_time = 1/(las_mult*7.5)
#			get_node("Timer").start()
#			laser_sound.play()
#			shake(15)
		
		HP = clamp(HP,-1,MaxHP)
#		if HP < 0:
#			die()
#			HP = 0
			
		if shake && shake_num > 0:
			shake_num -= 1
			cam.offset = Vector2(rand_range(-shake_amnt, shake_amnt), rand_range(-shake_amnt, shake_amnt))
		else:
			cam.offset = Vector2(0,0)
		
		vel += acc * delta
		
		pos += vel * delta
		position = pos
		
		if get_node("SpriteFlash").modulate.a > 0:
			get_node("SpriteFlash").modulate.a -= 0.025
	
func shake(intensity):
	shake = true
	shake_num = 20
	shake_amnt = intensity
	
func hurt(amnt):
	if vul:
		HP -= amnt
		shake(50)
		get_node("SpriteFlash").modulate.a = 2
		get_node("Spark").emitting = true
		get_node("Timer3").start()
		hit_sound.play()
		scrape_sound.play()
		emit_signal("hurt")

func fire_cooldown():
	canfire = true
	
#func die():
#	var boom = explosion.instance()
#	level.death_pos = global_position
#	boom.position = global_position
#	FX_LAYER.add_child(boom)
#	get_node("Timer2").start()

func death_timer():
	emit_signal("death")
	

func hurt_timer_out():
	vul = true
	get_node("Spark").emitting = false
