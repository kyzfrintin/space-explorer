extends Node

var system : PackedScene
var system_stats : Dictionary = {sname = "null"}
var system_surfaces : Dictionary
var surface : Node
var surface_stats : Dictionary
var player_stats : Dictionary
var sys_path = "user://galaxy/"

onready var surface_res = preload("res://scenes/surface.tscn")
onready var system_res = preload("res://scenes/sun.tscn")

func system_loaded(system):
	if system_stats["sname"] == "null":
		system.generate()
	else:
		system.recreate(system_stats)
	var cam = system.game.get_node("player/Camera2D")
	cam.limit_left = system.game.space_cam_limits[0]
	cam.limit_top = system.game.space_cam_limits[1]
	cam.limit_right = system.game.space_cam_limits[2]
	cam.limit_bottom = system.game.space_cam_limits[3]

func surface_loaded(planet):
	get_node("../surface/ship").cam.limit_left = -100000
	get_node("../surface/ship").cam.limit_top = -100000
	get_node("../surface/ship").cam.limit_right = 100000
	get_node("../surface/ship").cam.limit_bottom = 100000

func system_exists(sname):
	var dir = Directory.new()
	dir.open(sys_path)
	return dir.dir_exists(sname)

func planet_exists(pname):
	var bodies = system_stats["bodies"]
	for i in bodies:
		if i["body_type"] == "planet":
			if i["planet_name"] == pname:
				return true
	return false

func land(planet):
	var dir = Directory.new()
	system_stats = planet.sun.save_data()
	var sname = system_stats['sname']
	var pname = planet.name
	
	var sys_exists = dir.dir_exists(sys_path)
	
	if !sys_exists:
		dir.open("user://")
		dir.make_dir('galaxy')

	dir.open(sys_path)
	
	if !system_exists(sname):
		dir.make_dir(sname)
		
	var data = File.new()
	var system_path = (sys_path + '/' + sname + '/' + sname + '.star')
	data.open(system_path, File.WRITE)
	data.store_var(system_stats)
	data.close()
	
	var p_exists = dir.file_exists(pname)
	var surf = surface_res
	
	if !p_exists:
		surface_stats = planet.planet_stats
	else:
		var pd = File.new()
		var p_path = (sys_path + '/' + sname + '/' + pname + '.planet')
		pd.open(p_path, File.READ)
		surface_stats = pd.get_var()
		pd.close()
	
	$landing.play()
	get_tree().change_scene_to(surf)


func liftoff(planet):
	var pname = planet.planet_name
	var sname = system_stats["sname"]
	var data = File.new()
	var system_path = (sys_path + '/' + sname + '/' + pname + '.planet')
	data.open(system_path, File.WRITE)
	data.store_var(planet.surface_stats)
	data.close()
	get_tree().change_scene("res://scenes/root.tscn")
	$liftoff.play()