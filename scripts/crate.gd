extends Node2D

var hp = 100
var size : float
var colliding = false
var type = "ast"

onready var game
onready var splode_fx_res = preload("res://scenes/block_splode.tscn")

#func _enter_tree():
#	$splode.spawn_node = get_node("../../../../sounds")
#	$ding.spawn_node = get_node("../../../../sounds")
#	var bs = 8/size
#	bs = clamp(bs, 0.1,8)
#	var ds = 6/size
#	ds = clamp(ds,0.1,8) 
#	$splode/boom.pitch_scale = bs
#	$ding/ding.pitch_scale = ds
	
func _ready():
	hp *= (size * rand_range(0.8,1.2))
	rotation = rand_range(0,360)

func _process(delta):
	if $Area2D.monitoring:
		var c = $Area2D.get_overlapping_bodies().size()
		if c > 0:
			if !colliding:
				$ding.play()
				colliding = true
		else:
			colliding = false

func spawn_splode_fx():
	var s_fx = splode_fx_res.instance()
	game.get_node("fx").add_child(s_fx)
	s_fx.position = global_position
	var s_scale = 2 * size
	s_fx.scale = Vector2(s_scale,s_scale)
	s_fx.emitting = true
	
func splode():
	$splode.play()
	spawn_splode_fx()
	game.shake(30)
	var s : int = round(size * rand_range(1.5,3.4))
	if s:
		var num = randi() % s
		if num > 0:
			game.spawn_asteroids(global_position, num, size*100, size, get_parent())
	queue_free()

func rescale(s):
	$Sprite.scale = Vector2(s,s)
	$Area2D/Shape.scale = Vector2(s,s)

func hit(damage,pos):
	hp -= damage
	if hp <= 0:
		splode()
	else:
		$hit.play()
		
