extends Node2D

var can_fire = true
var type = "null"
var fire_range = 35000
var dis
var spos
var ppos
var HP = 175
var drone_remain = 0
var max_drones = 15
var engaged = false

onready var game = get_node("../../../../..")
onready var player = game.get_node("player")
onready var missile_res = preload("res://scenes/missile.tscn")
onready var drone_res = preload("res://scenes/attack_drone.tscn")
onready var splode_res = preload("res://scenes/missile_explode.tscn")

func _process(delta):
	spos = global_position
	dis = spos.distance_to(player.global_position)
	ppos = (player.global_position + (player.vel))
	if dis < fire_range:
		$turret_pivot.look_at(ppos)
		if can_fire:
			attack()
		check_engagement()
	else:
		if engaged:
			engaged = false
			game.threat -= 10
			game.update_threat()
			if game.threat == 0:
				game.leave_battle()

func _ready():
	var c = rand_range(0,1)
	if c > 0.7:
		type = "Hangar"
		$Timer.wait_time = max_drones
		fire_range = 50000
		$turret_pivot/turret.queue_free()
	else:
		type = "Turret"
		$turret_pivot/bay.queue_free()
	
func _enter_tree():
	$boom.spawn_node = get_node("../../../../../sounds")

func hit(damage, pos):
	HP -= damage
	if HP <= 0:
		call_deferred("explode")

func explode():
	$boom.play()
	var splode = splode_res.instance()
	splode.position = global_position
	splode.friendly = false
	splode.damage = 100.0
	game.get_node("fx").add_child(splode)
	if engaged:
		game.threat -= 10
		game.update_threat()
		if game.threat == 0:
			game.leave_battle()
	queue_free()

func attack():
	match type:
		"Hangar":
			if game.drones < 15:
				launch_drones()
		"Turret":
			fire()

func check_engagement():
	if !engaged:
		engaged = true
		game.threat += 10
		game.update_threat()
		if !game.battle:
			game.enter_battle()

func fire():
	can_fire = false
	$Timer.start()
	var mis = missile_res.instance()
	var dir = spos.direction_to(ppos)
	mis.position = spos + Vector2(rand_range(-300,300),rand_range(-300,300))
	mis.dir = dir
	mis.damage = 50.0
	mis.friendly = false
	mis.target = ppos
	game.get_node("bullets").add_child(mis)

func launch_drones():
	can_fire = false
	$Timer.start()
	var num = 3 + randi() % 7
	if (game.drones + num) > max_drones:
		num = (max_drones - game.drones)
	drone_remain = num
	drone_away()

func drone_away():
	if drone_remain > 0:
		var drone = drone_res.instance()
		drone.position = spos
		drone.launch_vec = spos.direction_to(ppos)
		drone.add_to_group("enemies")
		game.get_node("enemies").add_child(drone)
		drone_remain -= 1
		$drone_cd.start()

func reload():
	can_fire = true
