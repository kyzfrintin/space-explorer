extends Particles2D

export(float) var timeout

func _ready():
	yield(get_tree().create_timer(timeout), "timeout")
	queue_free()
