extends Node2D

onready var bgs = [get_node("bg_parallax/nebula_far/1"),get_node("bg_parallax/nebula_far/2"),
get_node("bg_parallax/nebula_mid/2"),get_node("bg_parallax/nebula_mid/3"),
get_node("bg_parallax/nebula_close/3"),get_node("bg_parallax/nebula_close/4")]
var tweening_nebs = []
var colours = [Color(0.0562, 0.0258, 0.4140),Color(0.3803,0,0.0352),Color(0.0156,0.4627,0.4627),
Color(0.4274,0.0156,0.4627),Color(0.0705,0.4235,0.0117),Color(0.4117,0.4235,0.0117),
Color(0.482353, 0.1137, 0.6862),Color(0.871094, 0.2041, 0.3135),Color(0.898438, 0.4888, 0.2667)]

func _ready():
	for i in bgs:
		i.modulate = colours[randi() % colours.size()]
	tween_cam()

func tween_cam():
	var off = Vector2(rand_range(-1400000,1400000),rand_range(-1400000,1400000))
	var dur = rand_range(8,18)
	$cam_tween.interpolate_property($Camera2D, 'position', $Camera2D.position, off, dur, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$cam_tween.start()
	
func modulate_nebula():
	var neb = bgs[randi() % bgs.size()]
	var tweening = (tweening_nebs.find(neb,0) != -1)
	if !tweening:
		var col = colours[randi() % colours.size()]
		$modulate_tween.interpolate_property(neb,'modulate',neb.modulate,col,rand_range(15,40),Tween.TRANS_LINEAR,Tween.EASE_IN)
		$modulate_tween.start()
		$Timer.start(rand_range(10,20))
		tweening_nebs.append(neb)
	else:
		$bg_parallax/modulate_timer.start(1)

func end_modulate(object, key):
	tweening_nebs.remove(tweening_nebs.find(object))

func play():
	$MixingDeskMusic.queue_bar_transition("end")
	$MixingDeskMusic.play_mode = 0
	yield(get_tree().create_timer(5),"timeout")
	get_tree().change_scene("res://scenes/root.tscn")
	
func exit():
	get_tree().quit()

func _on_cam_tween_tween_completed(object, key):
	tween_cam()
