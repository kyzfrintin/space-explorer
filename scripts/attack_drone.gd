extends Node2D

onready var game = get_node("../..")
onready var player = get_node("../../player")
var spos
var ppos
var moving
var dis
var HP = 40
var active = false
var launch_vec 

func _ready():
	game.drones += 1
	game.threat += 1
	game.update_threat()
	$autoturret.target = player
	$autoturret.game = game
	
func _process(delta):
	spos = position
	ppos = player.position
	dis = spos.distance_to(ppos)
	if dis > 1000 and active:
		if !moving:
			move()
			moving = true
	if !active:
		position += (launch_vec * 30)

func hit(damage, pos):
	HP -= damage
	if HP <= 0:
		call_deferred("die")

func die():
	game.drones -= 1
	game.threat -= 1
	game.update_threat()
	if game.threat == 0:
		game.leave_battle()
	$boom2.play()
	$boom.emitting = true
	$dead_timer.start()
	$Sprite.queue_free()
	$Area2D.queue_free()
	$autoturret.queue_free()

func move():
	var target = ppos + Vector2(rand_range(-500,500),rand_range(-500,500))
	var time : = clamp((dis*0.0007), 0.2, 8)
	$Tween.interpolate_property(self, 'position', spos, target, time, Tween.TRANS_EXPO, Tween.EASE_OUT)
	$Tween.start()
	look_at(target)

func end_move(object, key):
	moving = false


func switch():
	active = true


func clear():
	queue_free()
